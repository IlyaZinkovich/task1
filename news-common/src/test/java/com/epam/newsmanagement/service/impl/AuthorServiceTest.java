package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.domain.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@ContextConfiguration(locations = {"classpath:spring-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {

    @Mock
    private AuthorDAO authorDAO;

    @InjectMocks
    private AuthorServiceImpl authorService;

    private Author testAuthor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        testAuthor = new Author(1l, "John", null);
    }

    @Test
    public void addAuthorSucceed() throws Exception {
        when(authorDAO.insert(testAuthor)).thenReturn(1l);
        long generatedId = authorService.addAuthor(testAuthor);
        assertThat(generatedId, is(greaterThan(0l)));
        verify(authorDAO).insert(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void editAuthorSucceed() throws Exception {
        authorService.editAuthor(testAuthor);
        verify(authorDAO).update(testAuthor);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void deleteAuthorSucceed() throws Exception {
        authorService.deleteAuthor(testAuthor.getId());
        verify(authorDAO).delete(testAuthor.getId());
        verify(authorDAO).deleteNewsAuthor(testAuthor.getId());
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void findByNewsIdSucceed() throws Exception {
        when(authorDAO.findByNewsId(1)).thenReturn(testAuthor);
        authorService.findByNewsId(1);
        verify(authorDAO).findByNewsId(1);
        verifyNoMoreInteractions(authorDAO);
    }

    @Test
    public void findByIdSucceed() throws Exception {
        when(authorDAO.findById(testAuthor.getId())).thenReturn(testAuthor);
        authorService.findById(testAuthor.getId());
        verify(authorDAO).findById(testAuthor.getId());
        verifyNoMoreInteractions(authorDAO);
    }
}
