package com.epam.newsmanagement.dao.impl;


import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.News;
import com.epam.newsmanagement.domain.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static com.epam.newsmanagement.dao.impl.DAOConstants.NEWS_ID;

public class NewsDAOImpl implements NewsDAO {

    private DataSource dataSource;

    private final static String INSERT_NEWS_QUERY = "INSERT INTO NEWS " +
            "(NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)" +
            " VALUES (NEWS_AI.NEXTVAL, ?, ?, ?, ?, ?)";
    private final static String INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR " +
            "(NEWS_ID, AUTHOR_ID)" +
            " VALUES (?, ?)";
    private final static String INSERT_NEWS_TAG_QUERY = "INSERT INTO NEWS_TAG " +
            "(NEWS_ID, TAG_ID)" +
            " VALUES (?, ?)";
    private final static String UPDATE_NEWS_QUERY = "UPDATE NEWS " +
        "SET SHORT_TEXT = ?, FULL_TEXT = ?, TITLE = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? " +
        "WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_TAG_QUERY = "DELETE NEWS_TAG WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_AUTHOR_QUERY = "DELETE NEWS_AUTHOR WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_COMMENTS_QUERY = "DELETE COMMENTS WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_QUERY = "DELETE NEWS WHERE NEWS_ID = ?";
    private final static String SELECT_ALL_NEWS_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, " +
            "NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE,COUNT(COMMENTS.comment_id) AS NumberOfComments " +
            "FROM NEWS " +
            "LEFT JOIN COMMENTS " +
            "ON COMMENTS.NEWS_ID=NEWS.NEWS_ID " +
            "GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, " +
            "NEWS.MODIFICATION_DATE ORDER BY NumberOfComments DESC";
    private final static String SELECT_NEWS_BY_ID_QUERY = "SELECT * FROM NEWS WHERE NEWS_ID = ?";
    private final static String FIND_NEWS_BY_AUTHOR_NAME_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, " +
            "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
            "FROM NEWS " +
            "INNER JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID " +
            "WHERE AUTHOR.AUTHOR_NAME = ?";
    private final static String FIND_NEWS_BY_AUTHOR_ID_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, " +
            "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, AUTHOR.AUTHOR_ID " +
            "FROM NEWS " +
            "INNER JOIN NEWS_AUTHOR ON NEWS_AUTHOR.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID " +
            "WHERE AUTHOR.AUTHOR_ID=?";
    private final static String FIND_NEWS_BY_TAG_NAME_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, " +
            "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
            "FROM NEWS " +
            "INNER JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE TAG.TAG_NAME = ?";
    private final static String FIND_NEWS_BY_TAG_ID_QUERY = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, " +
            "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
            "FROM NEWS " +
            "INNER JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE TAG.TAG_ID = ?";
    private final static String FIND_NEWS_BY_TAGS_NAME_QUERY_BEGIN = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, " +
            "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, " +
            "COUNT(TAG.TAG_ID) AS NumerOfTags " +
            "FROM NEWS INNER JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE TAG.TAG_NAME IN ";
    private final static String FIND_NEWS_BY_TAGS_NAME_QUERY_END = " GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, " +
            "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
            "ORDER BY NumerOfTags DESC";

    private final static String FIND_NEWS_BY_TAGS_ID_QUERY_BEGIN = "SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, " +
            "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, " +
            "COUNT(TAG.TAG_ID) AS NumerOfTags " +
            "FROM NEWS INNER JOIN NEWS_TAG ON NEWS_TAG.NEWS_ID = NEWS.NEWS_ID " +
            "INNER JOIN TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE TAG.TAG_ID IN ";
    private final static String FIND_NEWS_BY_TAGS_ID_QUERY_END = " GROUP BY NEWS.NEWS_ID, NEWS.SHORT_TEXT, " +
            "NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE " +
            "ORDER BY NumerOfTags DESC";

    private PreparedStatement prepareStatementForUpdate(Connection connection, News news) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS_QUERY);
        preparedStatement.setString(1, news.getShortText());
        preparedStatement.setString(2, news.getFullText());
        preparedStatement.setString(3, news.getTitle());
        preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
        preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
        preparedStatement.setLong(6, news.getId());
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsert(Connection connection, News news) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_QUERY, new String[]{NEWS_ID});
        preparedStatement.setString(1, news.getShortText());
        preparedStatement.setString(2, news.getFullText());
        preparedStatement.setString(3, news.getTitle());
        preparedStatement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
        preparedStatement.setDate(5, new Date(news.getModificationDate().getTime()));
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDelete(Connection connection, long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsAuthor(Connection connection, long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_AUTHOR_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsTag(Connection connection, long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsComments(Connection connection, long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_COMMENTS_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindById(Connection connection, long id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_NEWS_BY_ID_QUERY);
        preparedStatement.setLong(1, id);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindAll(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_NEWS_QUERY);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByAuthorName(Connection connection, String authorName) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_NEWS_BY_AUTHOR_NAME_QUERY);
        preparedStatement.setString(1, authorName);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByAuthorId(Connection connection, long authorId) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_NEWS_BY_AUTHOR_ID_QUERY);
        preparedStatement.setLong(1, authorId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByTagName(Connection connection, String tagName) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_NEWS_BY_TAG_NAME_QUERY);
        preparedStatement.setString(1, tagName);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByTags(Connection connection, List<Tag> tags) throws SQLException{
        String tagNames = tags.stream().map(t -> "?").collect(Collectors.joining(", "));
        String query = FIND_NEWS_BY_TAGS_NAME_QUERY_BEGIN +
                "(" + tagNames + ")" + FIND_NEWS_BY_TAGS_NAME_QUERY_END;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        for (int i = 0; i < tags.size(); i++) preparedStatement.setString(i + 1, tags.get(i).getName());
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByTagsId(Connection connection, List<Long> tagsId) throws SQLException{
        String tagNames = tagsId.stream().map(t -> "?").collect(Collectors.joining(", "));
        String query = FIND_NEWS_BY_TAGS_ID_QUERY_BEGIN +
                "(" + tagNames + ")" + FIND_NEWS_BY_TAGS_ID_QUERY_END;
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        for (int i = 0; i < tagsId.size(); i++) preparedStatement.setLong(i + 1, tagsId.get(i));
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindNewsByTagId(Connection connection, long tagId) throws SQLException{
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_NEWS_BY_TAG_ID_QUERY);
        preparedStatement.setLong(1, tagId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsertIntoNewsTag(Connection connection, long newsId, long tagId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_TAG_QUERY);
        preparedStatement.setLong(1, newsId);
        preparedStatement.setLong(2, tagId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsertIntoNewsAuthor(Connection connection, long newsId, long authorId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_AUTHOR);
        preparedStatement.setLong(1, newsId);
        preparedStatement.setLong(2, authorId);
        return preparedStatement;
    }

    private List<News> parseResultSetToList(ResultSet resultSet) throws SQLException {
        List<News> list = new LinkedList<>();
        while (resultSet.next()) {
            News news = new News();
            news.setId(resultSet.getLong("NEWS_ID"));
            news.setShortText(resultSet.getString("SHORT_TEXT"));
            news.setFullText(resultSet.getString("FULL_TEXT"));
            news.setTitle(resultSet.getString("TITLE"));
            news.setCreationDate(new java.util.Date(resultSet.getTimestamp("CREATION_DATE").getTime()));
            news.setModificationDate(new java.util.Date(resultSet.getDate("MODIFICATION_DATE").getTime()));
            list.add(news);
        }
        return list;
    }

    private News parseResultSetToObject(ResultSet resultSet) throws SQLException {
        News news = null;
        while(resultSet.next()) {
            news = new News();
            news.setId(resultSet.getLong("NEWS_ID"));
            news.setShortText(resultSet.getString("SHORT_TEXT"));
            news.setFullText(resultSet.getString("FULL_TEXT"));
            news.setTitle(resultSet.getString("TITLE"));
            news.setCreationDate(new java.util.Date(resultSet.getTimestamp("CREATION_DATE").getTime()));
            news.setModificationDate(new java.util.Date(resultSet.getDate("MODIFICATION_DATE").getTime()));
            break;
        }
        return news;
    }

    @Override
    public void insertNewsAuthor(long newsId, long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsertIntoNewsAuthor(connection, newsId, authorId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void insertNewsTag(long newsId, long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsertIntoNewsTag(connection, newsId, tagId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void insertNewsTags(long newsId, List<Long> tagIdList) throws DAOException {
        for (Long tagId : tagIdList) insertNewsTag(newsId, tagId);
    }

    @Override
    public News findById(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try(PreparedStatement preparedStatement = prepareStatementForFindById(connection, newsId);
            ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToObject(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public List<News> findByAuthor(String authorName) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByAuthorName(connection, authorName);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findByAuthor(long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByAuthorId(connection, authorId);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findByTag(String tagName) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByTagName(connection, tagName);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findByTag(long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByTagId(connection, tagId);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public long insert(News news) throws DAOException {
        long lastInsertId = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsert(connection, news)) {
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet != null && resultSet.next())
                    lastInsertId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return lastInsertId;
    }

    @Override
    public void update(News news) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForUpdate(connection, news)) {
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDelete(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsTag(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsTag(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsComments(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsComments(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsAuthor(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsAuthor(connection, newsId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindAll(connection);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findByTags(List<Tag> tags) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByTags(connection, tags);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> findByTagsId(List<Long> tagsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindNewsByTagsId(connection, tagsId);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}