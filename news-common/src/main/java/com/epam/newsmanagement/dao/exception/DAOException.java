package com.epam.newsmanagement.dao.exception;

import com.epam.newsmanagement.exception.NewsManagementException;

public class DAOException extends NewsManagementException {
    public DAOException() {
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
