package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.domain.Tag;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static com.epam.newsmanagement.dao.impl.DAOConstants.TAG_ID;

public class TagDAOImpl implements TagDAO {

    private DataSource dataSource;

    private final static String UPDATE_TAG_QUERY = "UPDATE TAG " +
            "SET TAG_NAME = ? " +
            "WHERE TAG_ID = ?";
    private final static String INSERT_TAG_QUERY = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (TAG_AI.NEXTVAL, ?)";
    private final static String DELETE_TAG_QUERY = "DELETE TAG WHERE TAG_ID = ?";
    private final static String DELETE_NEWS_TAG_QUERY = "DELETE NEWS_TAG WHERE TAG_ID = ?";
    private final static String SELECT_ALL_TAGS_QUERY = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG";
    private final static String SELECT_TAG_BY_ID_QUERY = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG WHERE TAG_ID = ?";
    private final static String SELECT_TAG_BY_NAME_QUERY = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG WHERE TAG_NAME = ?";
    private final static String SELECT_TAGS_BY_NEWS_ID_QUERY = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG " +
            "INNER JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID " +
            "WHERE NEWS_ID = ?";

    private PreparedStatement prepareStatementForUpdate(Connection connection, Tag tag) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG_QUERY);
        preparedStatement.setString(1, tag.getName());
        preparedStatement.setLong(2, tag.getId());
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForInsert(Connection connection, Tag tag) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TAG_QUERY, new String[]{TAG_ID});
        preparedStatement.setString(1, tag.getName());
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDelete(Connection connection, long tagId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TAG_QUERY);
        preparedStatement.setLong(1, tagId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForDeleteNewsTaq(Connection connection, long tagId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_TAG_QUERY);
        preparedStatement.setLong(1, tagId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindById(Connection connection, long id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAG_BY_ID_QUERY);
        preparedStatement.setLong(1, id);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindByNewsId(Connection connection, Long newsId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAGS_BY_NEWS_ID_QUERY);
        preparedStatement.setLong(1, newsId);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindAll(Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TAGS_QUERY);
        return preparedStatement;
    }

    private PreparedStatement prepareStatementForFindByName(Connection connection, String name) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAG_BY_NAME_QUERY);
        preparedStatement.setString(1, name);
        return preparedStatement;
    }

    private List<Tag> parseResultSetToList(ResultSet resultSet) throws SQLException {
        List<Tag> list = new LinkedList<>();
        while (resultSet.next()) {
            Tag tag = new Tag();
            tag.setId(resultSet.getLong("TAG_ID"));
            tag.setName(resultSet.getString("TAG_NAME"));
            list.add(tag);
        }
        return list;
    }

    private Tag parseResultSetToObject(ResultSet resultSet) throws SQLException {
        Tag tag = null;
        while(resultSet.next()) {
            tag = new Tag();
            tag.setId(resultSet.getLong("TAG_ID"));
            tag.setName(resultSet.getString("TAG_NAME"));
            break;
        }
        return tag;
    }

    @Override
    public List<Long> insert(List<Tag> tags) throws DAOException {
        List<Long> idList = new LinkedList<>();
        for (Tag t : tags) {
            long tagId = insert(t);
            idList.add(tagId);
        }
        return idList;
    }

    @Override
    public Tag findById(long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindById(connection, tagId);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToObject(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public long insert(Tag tag) throws DAOException {
        Tag foundTag = findByName(tag.getName());
        if (foundTag != null) return foundTag.getId();
        long lastInsertId = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForInsert(connection, tag)) {
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet != null && resultSet.next())
                    lastInsertId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return lastInsertId;
    }

    @Override
    public void update(Tag tag) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForUpdate(connection, tag)) {
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDelete(connection, tagId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> findAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindAll(connection);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> findByNewsId(long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindByNewsId(connection, newsId);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToList(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsTag(long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForDeleteNewsTaq(connection, tagId)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Tag findByName(String name) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = prepareStatementForFindByName(connection, name);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            return parseResultSetToObject(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
