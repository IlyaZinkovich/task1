package com.epam.newsmanagement.service.exception;

import com.epam.newsmanagement.exception.NewsManagementException;

/**
 * Created by Ilya_Zinkovich on 4/8/2015.
 */
public class ServiceException extends NewsManagementException {
    public ServiceException() {
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
