package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.domain.*;
import com.epam.newsmanagement.service.*;
import com.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class NewsManagementServiceImpl implements NewsManagementService {

    private static Logger logger = Logger.getLogger(NewsManagementServiceImpl.class);

    private NewsService newsService;
    private AuthorService authorService;
    private TagService tagService;
    private CommentService commentService;

    public NewsManagementServiceImpl() {
    }

    public NewsManagementServiceImpl(NewsService newsService, AuthorService authorService, TagService tagService, CommentService commentService) {
        this.newsService = newsService;
        this.authorService = authorService;
        this.tagService = tagService;
        this.commentService = commentService;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void addNewsAuthor(long newsId, Author author) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            long authorId = authorService.addAuthor(author);
            newsService.addNewsAuthor(newsId, authorId);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void addNewsTags(long newsId, List<Tag> tags) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            List<Long> tagIdList = tagService.addTags(tags);
            newsService.addNewsTags(newsId, tagIdList);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void addNewsTag(long newsId, Tag tag) throws ServiceException {
        if (newsService.findById(newsId) != null) {
            long tagId = tagService.addTag(tag);
            newsService.addNewsTag(newsId, tagId);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void deleteNewsWithComments(long newsId) throws ServiceException {
        newsService.deleteNews(newsId);
        List<Comment> commentsToDelete = commentService.findByNewsId(newsId);
        for (Comment comment : commentsToDelete)
            commentService.deleteComment(comment.getId());
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void addNewsDTO(News news, Author author, List<Tag> tags) throws ServiceException {
        long newsId = newsService.addNews(news);
        long authorId = authorService.addAuthor(author);
        newsService.addNewsAuthor(newsId, authorId);
        List<Long> tagIdList =  tagService.addTags(tags);
        newsService.addNewsTags(newsId, tagIdList);
    }

    @Override
    public NewsDTO findNewsDTOById(long newsId) throws ServiceException {
        NewsDTO complexNews = new NewsDTO();
        News news = newsService.findById(newsId);
        Author author = authorService.findByNewsId(newsId);
        List<Tag> tags = tagService.findByNewsId(newsId);
        List<Comment> comments = commentService.findByNewsId(newsId);
        complexNews.setNews(news);
        complexNews.setAuthor(author);
        complexNews.setTags(tags);
        complexNews.setComments(comments);
        return complexNews;
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
