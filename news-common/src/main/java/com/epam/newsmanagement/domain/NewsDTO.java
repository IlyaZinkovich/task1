package com.epam.newsmanagement.domain;

import java.io.Serializable;
import java.util.List;

/**
 * A value object consisting of news, author, list of tags
 * and list of comments
 */
public class NewsDTO implements Serializable {

    private static final long serialVersionUID = 2468570789620455148L;

    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public NewsDTO() {
    }

    public NewsDTO(News news, Author author, List<Tag> tags, List<Comment> comments) {
        this.news = news;
        this.author = author;
        this.tags = tags;
        this.comments = comments;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o != null && o.getClass() != this.getClass()) return false;

        NewsDTO that = (NewsDTO) o;

        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (news != null ? !news.equals(that.news) : that.news != null) return false;
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }
}
